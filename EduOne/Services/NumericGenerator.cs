﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EduOne
{
    public class NumericGeneratorService
    {
        /// <summary>
        ///  Factorial  of a number is obtained from the result of multiplying a series of descending natural numbers.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static int GenerateFactorial(int input)
        {
            var fact = input;
            for (var i = input - 1; i >= 1; i--)     
                fact *= i;
       
            return fact;
        }


        /// <summary>
        /// Entry point, all console calls 
        /// </summary>
        public static void Start()
        {
            Console.WriteLine("Enter a number to produce factorials of");
           
            try
            {
                var response = Console.ReadLine();
                Console.WriteLine("Produced factorial: {0}", ParseInput(response));
            }catch(Exception e)
            {
                Console.WriteLine("The following error occurred: {0} ", e.Message);
                Console.WriteLine("Please try again");

            }
            finally
            {
                //essentially this is recursion, however it is valid. We do not want the CLI program to quit unless instructed to do so
                Start();
            }
        }
        /// <summary>
        /// Parsing input response
        /// </summary>
        /// <param name="response">Response message | Input</param>
        /// <returns></returns>
        public static int ParseInput(string response)
        {
          
            var parsedInput = 0;

            //because we're only accepting non negative natural descending numeric values
            if (int.TryParse(response, out parsedInput))
            {
                if (parsedInput < 0)
                {
                    throw new ArgumentException("Input cannot be lower that 0");
                }
                if (parsedInput > 12)
                {
                    throw new OverflowException("Overflow occurs above 12 factorial");
                }
                parsedInput = GenerateFactorial(parsedInput);

                return parsedInput; 
            }
            throw new ArgumentException("You have entered an invalid value, please try again");
            
  
          
        }
    }
}
