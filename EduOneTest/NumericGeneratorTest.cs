﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace EduOneTest
{
    [TestClass]
    public class NumericGeneratorTest
    {
    
        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void Overflow()
        {
            EduOne.NumericGeneratorService.ParseInput("4000");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EmptyInput()
        {
            EduOne.NumericGeneratorService.ParseInput("");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InvalidInput()
        {
            EduOne.NumericGeneratorService.ParseInput("lkji1-pwqdjasl");
        }

        [TestMethod]
        public void ZeroTest()
        {
            long factorialOfZero = EduOne.NumericGeneratorService.ParseInput("0");
            Assert.AreEqual(0, factorialOfZero);
        }

        [TestMethod]
        public void FactorialOf5()
        {
            long factOf5 = EduOne.NumericGeneratorService.ParseInput("5");
          
            Assert.AreEqual(5 * 4 * 3 * 2 * 1, factOf5);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NegativeTest()
        {
             EduOne.NumericGeneratorService.ParseInput("-5");
          
        }
    }
 
}
